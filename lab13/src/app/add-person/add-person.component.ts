import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Person } from '../shared/person.model';
import { isNullOrUndefined, log } from 'util';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {
  @Output() addP = new EventEmitter<Object>();
  userForm: FormGroup;
  constructor() { }
  onAdd(name, surname, phone){
    if ((name.value!='') && (surname.value!='') && (phone.value!='')) {
      let man = {
        name_man: name.value,
        surname_man: surname.value,
        phone_man: phone.value
      }
      this.addP.emit(man);
      this.userForm.reset();
    }
    
  }
  
  ngOnInit() {
    this.userForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern(/[A-Za-zА-Яа-яёЁ]/)]),
    surname: new FormControl('', [Validators.required, Validators.pattern(/[A-Za-zА-Яа-яёЁ]/)]),
    phone: new FormControl('', [Validators.required, Validators.pattern(/[0-9]/), Validators.minLength(10), Validators.maxLength(11)])
  });
  }


}

