import { Component, OnInit } from '@angular/core';
import { Person } from './shared/person.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  searchString='';
  title = 'lab13';
  persons: Person[] = [];
  ngOnInit() {
    this.persons.push(new Person(1, 'Иван', 'Иванов', 9012345678));
    this.persons.push(new Person(2, 'Василий', 'Васильев', 9012345678));
    this.persons.push(new Person(3, 'Петр', 'Петров', 9012345678));
    this.persons.push(new Person(4, 'Павел', 'Павлов', 9012345678));
    this.persons.push(new Person(5, 'Михаил', 'Михайлов', 9012345678));
    this.persons.push(new Person(6, 'Александр', 'Александров', 9012345678));
    this.persons.push(new Person(7, 'Роман', 'Романов', 9012345678));
    this.persons.push(new Person(8, 'Алексей', 'Алексеев', 9012345678));
    this.persons.push(new Person(9, 'Кирилл', 'Кириллов', 9012345678));
    this.persons.push(new Person(10, 'Олег', 'Олегов', 9012345678));
    
  }
  changePersons(person){
    this.searchString='';
    this.persons.splice(this.persons.findIndex(human => human.id == person.id), 1, person);
  }
  deletePersons(id){
    this.searchString='';
    this.persons.splice(this.persons.findIndex(human => human.id == id), 1);
  }
  addPersons(obj){
    let id = +this.persons[this.persons.length-1].id +1;
    this.searchString='';
    this.persons.push(new Person(id, obj.name_man, obj.surname_man, obj.phone_man ));
  }
    }
